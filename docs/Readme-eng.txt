Thank you for choosing the X-CSL Package!

The X-CSL-Package project is a centralized CSL model library for displaying IVAO traffic in X-Plane.
The project is developed, maintained and served by the X-AiR and StepToSky Teams.
It can be installed and continuously updated using this X-CSL-Updater software.

- The X-CSL-Package is developed for and compatible with IVAO clients: X-IvAp and Altitude.

- This X-CSL-Updater is compatible Only with the Altitude IVAO client. If you still use X-IvAp download an older version of X-CSL-Updater (1.2.0 or older)

- The X-CSL-Package is not compatible with any other CSL libraries or separate CSL models, 
so please remove ALL files from your CSL library folder before using our library. 
If you are going ONLY to test our library - save a copy of your current CSL folder somewhere aside.

- The X-CSL-Package is not compatible with other virtual aviation network clients, but even in this case you are able to try to use our library. 
Use Menu -> Set Custom Path setting for set a custom location of CSL models.
If you are going ONLY to test our library - save a copy of your current CSL folder somewhere aside.

Please, read the instruction for downloading and installation!

Short instruction on how to start using the software:
1. Download zip-archive (tar.gz for Linux, dmg-image for MacOS) according to your operational system.
2. Unpack X-CSL-Updater folder from archive into any place or mount the dmg image and copy X-CSL-Updater app from the image to the Applications folder on MacOS.
3. To start the program:
 * for Windows-> Start X-CSL-Updater.exe
 * for Linux-> Start X-CSL-Updater.sh
 * for Mac-> Start X-CSL-Updater application
4. In the opened window press the "Browse" button and specify the X-Plane executable file.
5. Press the "Index" button to compare your local csl library with the remote csl library provided by the server to determine which csl packages should be updated.
6. When indexation process is completed, you will be able to see which packages are out-of-date in the package list.
7. To update outdated packages, select such packages (for select two or more lines - keep Ctrl-key pressed or press the "Select All" button to select all the packages) and press the "Update" button.



Use this form to contact us: https://csl.x-air.ru/contact/?lang_id=43

Official X-CSL-Package project web site (https://csl.x-air.ru/?lang_id=43)
Copyright © 2009-2020 X-AiR Team (https://x-air.ru)
Copyright © 2009-2020 StepToSky Team (https://steptosky.com)