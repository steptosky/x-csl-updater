<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU" sourcelanguage="en">
<context>
    <name>About</name>
    <message>
        <location filename="about.ui" line="35"/>
        <source>X-CSL-Updater :: About</source>
        <translation>X-CSL-Updater :: О программе</translation>
    </message>
    <message>
        <location filename="about.ui" line="58"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;More info at &lt;a href=&quot;http://www.steptosky.com&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;www.steptosky.com&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="about.ui" line="90"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;More info at &lt;a href=&quot;http://www.x-air.ru&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;www.x-air.ru&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="about.ui" line="151"/>
        <source>X-CSL-Updater ver.: </source>
        <translation>X-CSL-Updater ver.: </translation>
    </message>
    <message>
        <location filename="about.ui" line="182"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;The X-CSL-Package project is a centralized CSL model library for displaying &lt;a href=&quot;https://ivao.aero&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;IVAO&lt;/span&gt;&lt;/a&gt; traffic in &lt;a href=&quot;https://x-plane.com&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;X-Plane&lt;/span&gt;&lt;/a&gt;.&lt;br /&gt;The project is developed, maintained and served by the X-AiR and StepToSky Teams.&lt;br /&gt;It can be installed and continuously updated using this X-CSL-Updater.&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;right&quot; style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Official X-CSL-Package project web site (&lt;a href=&quot;https://csl.x-air.ru&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;https://csl.x-air.ru&lt;/span&gt;&lt;/a&gt;)&lt;br /&gt;Copyright © 2009-2020 X-AiR Team (&lt;a href=&quot;https://x-air.ru&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;https://x-air.ru&lt;/span&gt;&lt;/a&gt;)&lt;br /&gt;Copyright © 2009-2020 StepToSky Team (&lt;a href=&quot;https://steptosky.com&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;https://steptosky.com&lt;/span&gt;&lt;/a&gt;)&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>IndexStep</name>
    <message>
        <location filename="IndexStep.cpp" line="60"/>
        <source>Indexing, please wait...</source>
        <translation>Индексация, пожалуйста ждите...</translation>
    </message>
    <message>
        <location filename="IndexStep.cpp" line="125"/>
        <source>Indexing local files is successfully done.</source>
        <translation>Индексация локальных файлов завершена.</translation>
    </message>
    <message>
        <location filename="IndexStep.cpp" line="131"/>
        <source>To make all the packages up-to-date you have to download %1 of %2</source>
        <translation>Для полного обновления вам надо загрузить %1 из %2</translation>
    </message>
    <message>
        <location filename="IndexStep.cpp" line="132"/>
        <source>Select the packages you want to update and click &quot;Update&quot;.</source>
        <translation>Выберите пакет(ы), которые хотите обновить  и нажмите &quot;Обновить&quot;.</translation>
    </message>
    <message>
        <location filename="IndexStep.cpp" line="135"/>
        <source>Congratulations! All the packages are fully up-to-date.</source>
        <translation>Поздравляем! Все пакеты обновлены.</translation>
    </message>
    <message>
        <location filename="IndexStep.cpp" line="143"/>
        <source>Error: Cannot get indexing successfully done! See log file for details.</source>
        <translation>Ошибка: Не могу выполнить индексацию. Смотрите лог-файл, чтобы узнать больше.</translation>
    </message>
    <message>
        <location filename="IndexStep.cpp" line="165"/>
        <source>Please wait...</source>
        <translation>Пожалуйста ждите...</translation>
    </message>
    <message>
        <location filename="IndexStep.cpp" line="442"/>
        <source>Indexing process has been canceled by user!</source>
        <translation>Индексация отменена пользователем!</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="44"/>
        <source>X-CSL-Updater</source>
        <translation>X-CSL-Updater</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="90"/>
        <source>X-CSL-Package logo</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="171"/>
        <source>X-Plane dir:</source>
        <translation>Путь к X-Plane:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="199"/>
        <source>UNDEFINED...</source>
        <translation>Путь не определён...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="218"/>
        <location filename="mainwindow.ui" line="586"/>
        <source>Specify the X-Plane executable file location</source>
        <translation>Укажите на исполняемый файл X-Plane</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="221"/>
        <source>Browse</source>
        <translation>Обзор</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="244"/>
        <source>log</source>
        <translation>Лог</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="301"/>
        <source>X-CSL-Package list available on the server:</source>
        <translation>Список CSL моделей на сервере:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="334"/>
        <source>Select packages you want to update and click Update button.</source>
        <translation>Выберите пакет, кторый вы хотите обновить и нажмите кнопку Обновить.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="383"/>
        <source>#ID</source>
        <translation>#ID</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="388"/>
        <source>Name</source>
        <translation>Имя</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="393"/>
        <location filename="mainwindow.cpp" line="38"/>
        <source>Info</source>
        <translation>Инфо</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="398"/>
        <source>Version</source>
        <translation>Версия</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="403"/>
        <source>Size</source>
        <translation>Размер</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="408"/>
        <source>Status</source>
        <translation>Статус</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="413"/>
        <source>Code</source>
        <translation>Код</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="435"/>
        <source>Select all packages to be updated (Ctrl+A)</source>
        <translation>Выделить все пакеты для обновления (Ctrl+A)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="438"/>
        <location filename="mainwindow.cpp" line="30"/>
        <location filename="mainwindow.cpp" line="35"/>
        <source>Select All</source>
        <translation>Выделить всё</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="470"/>
        <source>Cancel the current operation</source>
        <translation>Отмена текущей  операции</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="473"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="498"/>
        <source>Index local files to determine the files need to be updated (Ctrl+I)</source>
        <translation>Индексация локальных пакетов для обновления (Ctrl+I)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="501"/>
        <source>Index</source>
        <translation>Индексировать</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="504"/>
        <source>Ctrl+I</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="523"/>
        <source>Update selected packages (Ctrl+U)</source>
        <translation>Обновить выделенные пакеты (Ctrl+U)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="526"/>
        <source>Update</source>
        <translation>Обновить</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="529"/>
        <source>Ctrl+U</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="554"/>
        <source>Menu</source>
        <translation>Меню</translation>
    </message>
    <message>
        <source>File</source>
        <translation type="vanished">Меню</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="570"/>
        <source>Help</source>
        <translation>Помощь</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="580"/>
        <location filename="mainwindow.ui" line="583"/>
        <source>Set X-Plane location</source>
        <translation>Установить путь к X-Plane</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="592"/>
        <source>Ctrl+O</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="600"/>
        <location filename="mainwindow.ui" line="603"/>
        <source>Settings</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="611"/>
        <source>Exit</source>
        <translation>Выход</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="614"/>
        <source>Ctrl+Q</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="619"/>
        <source>About</source>
        <translation>О программе</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="622"/>
        <source>About this program</source>
        <translation>Об этой программе</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="628"/>
        <source>Ctrl+/</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="633"/>
        <source>About Qt</source>
        <translation>О Qt</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="638"/>
        <source>Set Custom Path</source>
        <translation>Установить путь для CSL (!)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="28"/>
        <source>Clear</source>
        <translation>Очистить</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="31"/>
        <location filename="mainwindow.cpp" line="36"/>
        <source>Ctrl+A</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="55"/>
        <source>, Ver.:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="222"/>
        <source> :: ERROR!</source>
        <translation> :: ОШИБКА!</translation>
    </message>
    <message>
        <source> :: Command line usage:</source>
        <translation type="vanished"> :: Параметры запуска из сомандной строки:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="196"/>
        <location filename="mainwindow.cpp" line="201"/>
        <location filename="mainwindow.cpp" line="206"/>
        <source> :: Specify the X-Plane executable file location ...</source>
        <translation> :: Укажите расположение исполняемого файла X-Plane ...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="223"/>
        <source>The specified X-Plane executable file path is not valid!
Or the X-Plane installation located at the specified path is not valid or broken.
You can try to reinstall or repair your X-Plane installation.</source>
        <translation>Указанный путь до исполняемого файла X-Plane не валиден!
Или инсталяция X-Plane по указанному пути не валидна или сломана.
Вы можете попробовать переустановить X-Plane.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="110"/>
        <location filename="mainwindow.cpp" line="256"/>
        <source>Custom dir: </source>
        <translation>Путь для CSL (!): </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="113"/>
        <location filename="mainwindow.cpp" line="259"/>
        <source>X-Plane dir: </source>
        <translation>Путь к X-Plane: </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="263"/>
        <source>Now click &quot;Index&quot; to determine files which need to be updated.</source>
        <translation>Нажмите &quot;Индексировать&quot; для определения пакетов, требующих установки/обновления .</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="282"/>
        <source>Cannot create temporary folder: </source>
        <translation>Не могу создать временную папку: </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="292"/>
        <source>Please specify the X-Plane executable file location!

Navigate to the folder where your X-Plane is installed and select the X-Plane executable file.</source>
        <translation>Укажите расположение исполняемого файла X-Plane!

Перейдите в папку, где установлен X-Plane, и выберите исполняемый файл X-Plane.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="334"/>
        <source>Warning! This function is designed for advanced users.
Please use it only if you are absolutely sure what you are doing otherwise the program can become unusable!
Note: no additional files will be installed/updated, only the X-CSL library files will be installed/updated.</source>
        <translation>Внимание!!! Эта функция только для опытных пользователей
Пожалуйста используйте эту настройку только если абсолютно уверены в том, что делаете. Иначе программа может стать неработоспособной!

Важно! Никакие дополнительные файлы не будут установлены/обновлены,только пакеты CSL-моделей.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="339"/>
        <source> :: Specify a folder where the library will be installed</source>
        <translation> :: Укажите папку, в которую будут установлены пакеты CSL моделей</translation>
    </message>
</context>
<context>
    <name>PackageAdditionalInfo</name>
    <message>
        <location filename="PackageAdditionalInfo.ui" line="29"/>
        <source>X-CSL-Updater :: Info</source>
        <translation>X-CSL-Updater :: Инфо</translation>
    </message>
    <message>
        <location filename="PackageAdditionalInfo.ui" line="40"/>
        <source>Info about the selected X-CSL Package:</source>
        <translation>Информация о пакете:</translation>
    </message>
    <message>
        <location filename="PackageAdditionalInfo.ui" line="53"/>
        <source>Info about the selected X-CSL Package</source>
        <translation>Информация о пакете</translation>
    </message>
    <message>
        <location filename="PackageAdditionalInfo.ui" line="85"/>
        <source>Click to close the window</source>
        <translation>Закрыть окно</translation>
    </message>
    <message>
        <location filename="PackageAdditionalInfo.ui" line="91"/>
        <source>OK</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PackageAdditionalInfo.cpp" line="94"/>
        <location filename="PackageAdditionalInfo.cpp" line="95"/>
        <source>There is no information...</source>
        <translation>Нет информации...</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="main.cpp" line="47"/>
        <source> :: ERROR!</source>
        <translation> :: ОШИБКА!</translation>
    </message>
    <message>
        <source>Cannot open log file: &lt;log.txt&gt;</source>
        <translation type="vanished">Не могу открыть лог-файл: &lt;log.txt&gt;</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="settings.ui" line="29"/>
        <source>X-CSL-Updater :: Settings</source>
        <translation>X-CSL-Updater :: Настройки</translation>
    </message>
    <message>
        <location filename="settings.ui" line="62"/>
        <source>Server</source>
        <translation>Сервер</translation>
    </message>
    <message>
        <location filename="settings.ui" line="80"/>
        <source>Server 1:</source>
        <translation>Сервер 1:</translation>
    </message>
    <message>
        <location filename="settings.ui" line="90"/>
        <source>Server address #1</source>
        <translation>Адрес сервера #1</translation>
    </message>
    <message>
        <location filename="settings.ui" line="100"/>
        <location filename="settings.ui" line="130"/>
        <location filename="settings.ui" line="160"/>
        <location filename="settings.ui" line="190"/>
        <source>The program will use the selected server</source>
        <translation>Программа будет использовать указанный сервер</translation>
    </message>
    <message>
        <location filename="settings.ui" line="103"/>
        <location filename="settings.ui" line="133"/>
        <location filename="settings.ui" line="163"/>
        <location filename="settings.ui" line="193"/>
        <source>Use</source>
        <translation>Использовать</translation>
    </message>
    <message>
        <location filename="settings.ui" line="113"/>
        <source>Server 2:</source>
        <translation>Сервер 2:</translation>
    </message>
    <message>
        <location filename="settings.ui" line="120"/>
        <source>Server address #2</source>
        <translation>Адрес сервера #2</translation>
    </message>
    <message>
        <location filename="settings.ui" line="143"/>
        <source>Server 3:</source>
        <translation>Сервер 3:</translation>
    </message>
    <message>
        <location filename="settings.ui" line="150"/>
        <source>Server address #3</source>
        <translation>Адрес сервера #3</translation>
    </message>
    <message>
        <location filename="settings.ui" line="173"/>
        <source>Server 4:</source>
        <translation>Сервер 4:</translation>
    </message>
    <message>
        <location filename="settings.ui" line="180"/>
        <source>Server address #4</source>
        <translation>Адрес сервера #4</translation>
    </message>
    <message>
        <location filename="settings.ui" line="205"/>
        <source>Lang</source>
        <translation>Язык</translation>
    </message>
    <message>
        <location filename="settings.ui" line="220"/>
        <source>Русский</source>
        <translation></translation>
    </message>
    <message>
        <location filename="settings.ui" line="230"/>
        <source>English</source>
        <translation></translation>
    </message>
    <message>
        <location filename="settings.ui" line="264"/>
        <source>Click to cancel changes</source>
        <translation>Нажмите для отмены изменений</translation>
    </message>
    <message>
        <location filename="settings.ui" line="267"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <location filename="settings.ui" line="280"/>
        <source>Click to save changes</source>
        <translation>Нажмите для сохранения изменений</translation>
    </message>
    <message>
        <location filename="settings.ui" line="283"/>
        <source>OK</source>
        <translation></translation>
    </message>
    <message>
        <location filename="settings.cpp" line="62"/>
        <location filename="settings.cpp" line="70"/>
        <location filename="settings.cpp" line="74"/>
        <location filename="settings.cpp" line="78"/>
        <location filename="settings.cpp" line="82"/>
        <location filename="settings.cpp" line="86"/>
        <location filename="settings.cpp" line="122"/>
        <source>http://csl.x-air.ru/package/</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>UpdateStep</name>
    <message>
        <location filename="UpdateStep.cpp" line="18"/>
        <source>The operation has been canceled by user!</source>
        <translation>Операция отменена пользователем!</translation>
    </message>
    <message>
        <location filename="UpdateStep.cpp" line="107"/>
        <source>Updating, please wait...</source>
        <translation>Обновление, пожалуйста ждите...</translation>
    </message>
    <message>
        <location filename="UpdateStep.cpp" line="167"/>
        <source>Error: Cannot get updating successfully done! The X-CSL library can be broken! See log file for details.</source>
        <translation>Ошибка: Не могу выполнить обновление! X-CSL библиотека может быть повреждёна! См. лог-файл.</translation>
    </message>
    <message>
        <location filename="UpdateStep.cpp" line="168"/>
        <source>Reindexing is required to continue. Please click &quot;Index&quot; button.</source>
        <translation>Переиндексация необходима чтобы продолжить.Нажмите кнопку «Индексировать».</translation>
    </message>
    <message>
        <location filename="UpdateStep.cpp" line="174"/>
        <source>Updating process is successfully done!</source>
        <translation>Обновление успешно завершено!</translation>
    </message>
    <message>
        <location filename="UpdateStep.cpp" line="204"/>
        <source>Updating: %1...</source>
        <translation>Обновление: %1 ...</translation>
    </message>
</context>
</TS>
